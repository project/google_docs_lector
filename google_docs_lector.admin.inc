<?php

/**
 * @file
 * Admin file for Google Docs Lector
 */

/**
 * Settings for google docs lector file.
 */
function google_docs_lector_admin_settings() {
  $form['about'] = array(
    '#markup' => t('Configure Google Docs Lector'),
  );
  return system_settings_form($form, FALSE);
}