
README.txt
==========

Usage
=====

Create or attach an existing field into an Entity and select the formatter value to Google Docs Lector.

Select settings to adjust the width and height to your needs.

Additional Support
=================
For support, please create a support request for this module's project:
  http://drupal.org/project/

Support questions by email to the module maintainer will be simply ignored. Use the issue tracker.

====================================================================
Victor Castell, victor.castell at season.es, http://season.es